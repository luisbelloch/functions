PROJECT_ID?=bigdataupv2022
REGION?=us-east1
FUNCTION_URL:=https://$(REGION)-$(PROJECT_ID).cloudfunctions.net/$(FUNCTION_NAME)
HTTPIE:=docker run -ti luisbelloch/httpie
CLOUD_RUN_URL:=gcloud functions describe $(FUNCTION_NAME) --region $(REGION) --format="value(serviceConfig.uri)"

current_makefile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
current_dir := $(dir $(current_makefile_path))

.PHONY: help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(current_makefile_path) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(firstword $(MAKEFILE_LIST)) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

.PHONY: list
list: ## List all cloud functions deployed in region
	gcloud functions list

.PHONY: describe
describe: ## Get information about one function
	gcloud functions describe $(FUNCTION_NAME) --region $(REGION)

.PHONY: delete
delete: ## Removes deployment
	gcloud functions delete --region $(REGION) --quiet $(FUNCTION_NAME)

.PHONY: deploy
deploy: ## Deploys function
	gcloud functions deploy $(FUNCTION_NAME) $(GEN) --runtime python39 --trigger-http --allow-unauthenticated --project $(PROJECT_ID) --region $(REGION) $(DEPLOY_PARAMS)

.PHONY: deploy_gen2
deploy_gen2: GEN=--gen2
deploy_gen2: deploy ## Deploys function using 2nd generation environment

.PHONY: test
test: ## Runs tests locally using pytest
	python -m pytest -v

# python3 -m venv .venv
# source .venv/bin/activate
.PHONY: dependencies
dependencies: ## Installs python dependencies locally
	pip install --upgrade pip
	pip install -r requirements.txt -r requirements-dev.txt

.PHONY: enable-apis
enable_apis: ## Enables Cloud Functions API and dependencies
	gcloud services enable cloudfunctions.googleapis.com cloudbuild.googleapis.com run.googleapis.com artifactregistry.googleapis.com
